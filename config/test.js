const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin')
const WebpackShellPlugin = require('webpack-shell-plugin');
const merge = require("webpack-merge")
//const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
const nodeExternals = require('webpack-node-externals')

//const VueLoaderPlugin = require('vue-loader/lib/plugin')

//module.exports = {
//  mode: 'development',
//		entry: path.resolve(__dirname + '/../test/test.js'),
//		output: {
//			path: path.resolve(__dirname + '/../dist-test/'),
//			filename: 'test-bundle.js'
//		},
//  module: {
//    rules: [
//      {
//        test: /\.vue$/,
//        loader: 'vue-loader'
//      },
//      // this will apply to both plain `.js` files
//      // AND `<script>` blocks in `.vue` files
//      {
//        test: /\.js$/,
//        loader: 'babel-loader'
//      },
//      // this will apply to both plain `.css` files
//      // AND `<style>` blocks in `.vue` files
//      {
//        test: /\.css$/,
//        use: [
//          'vue-style-loader',
//          'css-loader'
//        ]
//      }
//    ]
//  },
//  plugins: [
//    // make sure to include the plugin for the magic
//    new VueLoaderPlugin()
//  ]
//}

module.exports = function(configDirs) {
	console.log('\x1b[36m%s\x1b[0m', 'Running testing ...');

	return merge(require('./common-test.js')(), {
		entry: path.resolve(__dirname + '/../test/test.js'),
		output: {
			library: 'vue_u16n_test',
			libraryTarget: 'umd',
//			filename: "[name].js",
			auxiliaryComment: 'vue-u16n test bundle',
			filename: 'u16n.vue.test.js',
			devtoolModuleFilenameTemplate: '[absolute-resource-path]',
			devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'/*,
			globalObject: "this"*/
		},
		externals: [nodeExternals()],
		plugins: [
//			new StaticSiteGeneratorPlugin({
//				globals: {
//					window: {}
//				}
//			}),
//			new WebpackShellPlugin({
//				//onBuildExit: "mocha "+path.resolve(__dirname + "/../dist-test/testBundle.js")+" --inline-diffs --reporter mocha-simple-html-reporter --reporter-options output=dist-test/report/simple/report.html",
//				onBuildExit: "mocha "+path.resolve(__dirname + "/../dist-test/u16n.vue.test.js")+" --inline-diffs --reporter mochawesome --reporter-options reportDir=dist-test/report/mochawesome"
//			})
		]
	});

};

