const path = require('path');
const merge = require("webpack-merge")
const TerserPlugin = require('terser-webpack-plugin')

module.exports = function(configDirs) {
	return merge(require('./common-base.js'), {

		output: {
			path: path.resolve(__dirname + '/../dist/'),
			library: 'vueYcon',
			libraryTarget: 'umd',
			filename: "[name].js",
			auxiliaryComment: 'vue-ycon plugin'
		},
		devtool: "source-map",
		optimization: {
			minimizer: [
				// we specify a custom UglifyJsPlugin here to get source maps in production
				new TerserPlugin({
					include: /\.min\.js$/,
					parallel: true,
					terserOptions: {
						ecma: 6,
					},
				})
			]
		},
		plugins: [
		]
	});
};
