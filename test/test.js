//import Vue from 'vue'
//const Vue = require ('vue')
import { mount, shallowMount, config } from '@vue/test-utils'
import icon from '../src/component/icon.vue'
const prettifyHtml = require('prettify-html')


function iconToHTML(props) {
	return prettifyHtml(mount(icon, {
		propsData: props
	}).html())
}

describe('simple icon', () => {

	it ('renders simple icon', () => {
		expect(iconToHTML({
			name: 'foo'
		})).to.equal('<i class="fas fa-foo"></i>')
	})

	it ('renders icon with color', () => {
		expect(iconToHTML({
			name: 'foo',
			color: 'green'
		})).to.equal('<i class="fas fa-foo" style="color: green;"></i>')
	})

	it ('renders icon with color and border', () => {
		expect(iconToHTML({
			name: 'foo',
			color: 'green',
			borderColor: 'grey'
		})).to.equal('<i class="fas fa-foo" style="color: green; text-shadow: -1px 0 grey, 0 1px grey, 1px 0 grey, 0 -1px grey;"></i>')
	})

	it ('renders icon zoomed by 3', () => {
		expect(iconToHTML({
			name: 'foo',
			zoom: 3
		})).to.equal('<i class="fas fa-foo" zoom="3"></i>')
	})

	it ('renders icon with custom classes', () => {
		expect(iconToHTML({
			name: 'foo',
			clazz: ['selected']
		})).to.equal('<i class="fas fa-foo selected"></i>')

		expect(iconToHTML({
			name: 'foo',
			clazz: 'selected'
		})).to.equal('<i class="fas fa-foo selected"></i>')

		expect(iconToHTML({
			name: 'foo',
			clazz: ['selected', 'admin-only']
		})).to.equal('<i class="fas fa-foo selected admin-only"></i>')

		expect(iconToHTML({
			name: 'foo',
			clazz: 'selected admin-only'
		})).to.equal('<i class="fas fa-foo selected admin-only"></i>')
	})
})

describe('icon with sub icon', () => {

	it ('renders icon with sub icon on bottom right', () => {
		expect(iconToHTML({
			name: 'foo',
			sub: {
				name: 'bar',
				position: 'b/r'
			}
		})).to.equal(
			['<span class="fa-stack" style="height: 1em; line-height: 1em; width: 1em;">',
			 '  <i class="fas fa-stack-1x fa-foo"></i>',
			 '  <i class="fas fa-stack-1x fa-bar" style="left: 0.5em; top: 0.5em;"></i>',
			 '</span>'].join("\n"))

	})
})

