vue-ycon
========

A vue library to display icons

# Install

~~~
npm i vue-ycon
~~~

# Usage

## Plugin installation

~~~javascript
import VueYcon from 'vue-ycon'

Vue.use(VueYcon)
~~~

## Examples

~~~html
<icon name="bowling-ball"></icon>
~~~

A red bomb :
~~~html
<icon name="bomb" style="color:red"></icon>
~~~

A book with a blue certificate on the bottom/right
~~~html
<icon name="book" :sub="{ name:'certificate', position:'b/r', color:'blue' }"></icon>
~~~
