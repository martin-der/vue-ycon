const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin')
const merge = require("webpack-merge")
//const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = function(configDirs) {
	console.log('\x1b[36m%s\x1b[0m', 'Building for production ...');

	return merge(require('./common-main.js')(), {
		entry: {
			"vue-ycon": path.resolve(__dirname + '/../src/vue-ycon.js'),
			"vue-ycon.min": path.resolve(__dirname + '/../src/vue-ycon.js')
		},
		output: {
		    library: 'vueYcon',
            libraryTarget: 'umd',
			filename: "[name].js",
            auxiliaryComment: 'Vue Icon plugin'
		},
		plugins: [
//			new HtmlWebpackPlugin({
//			  template: './src/index.html',
//			}),
		]
	});
};
