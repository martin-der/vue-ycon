require('jsdom-global')()

const chai = require('chai')
global.chai = chai
global.expect = chai.expect
global.should = chai.should()

const spies = require('chai-spies');
chai.use(spies);
