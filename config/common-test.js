const path = require('path');
const merge = require("webpack-merge")

module.exports = function(configDirs) {
	return merge(require('./common-base.js'), {

		mode: 'development',
		output: {
			path: path.resolve(__dirname + '/../dist-test/')
		},
		plugins: [
		]
	});
};
