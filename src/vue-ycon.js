import Vue from 'vue';
import icon from './component/icon.vue';

export default {
	install : function (Vue, options) {
		Vue.component('icon', icon);
	}
};
