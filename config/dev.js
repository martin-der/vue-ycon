const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin')
const merge = require("webpack-merge")

module.exports = function(configDirs) {
	console.log('\x1b[36m%s\x1b[0m', 'Building for development ...');

	return merge(require('./common-main.js'), {
		entry: path.resolve(__dirname + '/../src/vue-ycon.js'),
		output: {
			filename: 'vue-ycon.js'
		}
	});
};

